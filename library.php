<!DOCTYPE html>
<html>

<head>
    <title>e-Library</title>
    <?php include 'include/inc-head.php'; ?>
</head>

<body data-plugin-page-transition>

    <div class="body">
        <?php include 'include/inc-header.php'; ?>

        <div role="main" class="main">

            <section class="page-header page-header-modern mb-0">
                <div class="container-xxl">
                    <div class="row">
                        <div class="col-sm-6 align-self-center text-start">
                            <h1 class="text-dark">E-Library</h1>
                        </div>
                        <div class="col-sm-6 align-self-center text-start text-sm-end">
                            <ul class="breadcrumb d-block">
                                <li><a href="#">Home</a></li>
                                <li class="active">e-Library</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>

            <div class="container-xxl">
                <div class="row">

                    <div class="sidebar col-md-3 col-lg-2 pt-3">
                        <div class="container-fluid">
                            <p class="text-1 mb-0">CATEGORY</p>
                            <div class="sort-source nav-list" data-sort-id="category" data-option-key="filter" data-plugin-options="{  'layoutMode': 'fitRows', 'filter': '.e-book'}">
                                <button class="card card-profile-detail2 ">
                                    <li class="my-0" data-option-value=".e-book"><a href="#">E-book</a></li>
                                </button>
                                <button class="card card-profile-detail2">
                                    <li class="my-0" data-option-value=".video"><a href="#">Video</a></li>
                                </button>
                                <button class="card card-profile-detail2">
                                    <li class="my-0" data-option-value=".infographic"><a href="#">Infographic</a></li>
                                </button>
                                <button class="card card-profile-detail2">
                                    <li class="my-0" data-option-value=".se-ed-e-library"><a href="#">SE-ED E-Library</a></li>
                                </button>
                            </div>
                        </div>
                    </div>

                    <div class=" content col col-md-9 col-lg-10 pt-0">
                        <div class="container pb-5">
                            <div class="sort-destination-loader sort-destination-loader-showing">
                                <div class="row portfolio-list sort-destination g-5" data-sort-id="category">

                                    <!-- e-book -->
                                    <div class="col-sm-6 col-lg-4 isotope-item e-book">
                                        <div class="card">
                                            <a href="#"><img class="card-img-top" height="auto" src="img/1-main/book1.png" alt="online book"></a>
                                            <div></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-lg-4 isotope-item e-book">
                                        <div class="card">
                                            <a href="#"><img class="card-img-top" height="auto" src="img/1-main/book2.png" alt="online book"></a>
                                            <div></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-lg-4 isotope-item e-book">
                                        <div class="card">
                                            <a href="#"><img class="card-img-top" height="auto" src="img/1-main/book3.png" alt="online book"></a>
                                            <div></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-lg-4 isotope-item e-book">
                                        <div class="card">
                                            <a href="#"><img class="card-img-top" height="auto" src="img/1-main/book4.png" alt="online book"></a>
                                            <div></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-lg-4 isotope-item e-book">
                                        <div class="card">
                                            <a href="#"><img class="card-img-top" height="auto" src="img/1-main/book5.png" alt="online book"></a>
                                            <div></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-lg-4 isotope-item e-book">
                                        <div class="card">
                                            <a href="#"><img class="card-img-top" height="auto" src="img/1-main/book6.png" alt="online book"></a>
                                            <div></div>
                                        </div>
                                    </div>

                                    <!-- video -->
                                    <div class="col-sm-6 col-lg-4 isotope-item video">
                                        <div class="ratio ratio-4x3">
                                            <video id="video2" width="100%" height="100%" muted loop preload="metadata" poster="./video/presentation.jpg">
                                                <source src="./video/deves-video.mp4" type="video/mp4">
                                            </video>
                                            <a href="#" class="position-absolute top-50pct left-50pct transform3dxy-n50 bg-light rounded-circle d-flex align-items-center justify-content-center text-decoration-none bg-color-hover-primary text-color-hover-light play-button-lg pulseAnim pulseAnimAnimated" data-trigger-play-video="#video2" data-trigger-play-video-remove="yes">
                                                <i class="fas fa-play text-5"></i>
                                            </a>
                                        </div>
                                    </div>

                                    <!-- infographic -->
                                    <div class="col-sm-6 col-lg-4 isotope-item infographic">
                                        <div class="card">
                                            <a href="#"><img class="card-img-top" height="auto" src="img/projects/project-2.jpg" alt="online book"></a>
                                            <div class="thumb-deves"></div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <?php include 'include/inc-footer.php'; ?>
    </div>
    <?php include 'include/inc-script.php'; ?>

</body>

</html>