<!DOCTYPE html>
<html>

<head>
    <title>FAQ</title>
    <?php include 'include/inc-head.php'; ?>
</head>

<body data-plugin-page-transition>

    <div class="body">
        <?php include 'include/inc-header.php'; ?>

        <div role="main" class="main">

            <section class="page-header page-header-modern mb-0">
                <div class="container-xxl">
                    <div class="row">
                        <div class="col-sm-6 align-self-center text-start">
                            <h1 class="text-dark">FAQ</h1>
                        </div>
                        <div class="col-sm-6 align-self-center text-start text-sm-end">
                            <ul class="breadcrumb d-block">
                                <li><a href="#">Home</a></li>
                                <li class="active">FAQ</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>

            <div class="container content faq">
                <div class="toggle toggle-minimal" data-plugin-toggle>
                    <section class="toggle active">
                        <a class="toggle-title">1.) Study/exam through e-Learning system</a>
                        <div class="toggle-content">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur pellentesque neque eget diam posuere porta. Quisque ut nulla at nunc lacinia. Proin adipiscing porta tellus, ut feugiat nibh adipiscing sit amet.
                        </div>
                    </section>
                    <section class="toggle">
                        <a class="toggle-title">2.) forgot password</a>
                        <div class="toggle-content">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur eget leo at velit imperdiet varius. In eu ipsum vitae velit congue iaculis vitae at risus. Nullam tortor nunc, bibendum vitae semper a, volutpat eget massa. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer fringilla, orci sit amet posuere auctor.
                        </div>
                    </section>
                    <section class="toggle">
                        <a class="toggle-title">3.) How to report a problem</a>
                        <div class="toggle-content">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur eget leo at velit imperdiet varius. In eu ipsum vitae velit congue iaculis vitae at risus. Nullam tortor nunc, bibendum vitae semper a, volutpat eget massa. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer fringilla, orci sit amet posuere auctor, orci eros pellentesque odio, nec pellentesque erat ligula nec massa. Aenean consequat lorem ut felis ullamcorper posuere gravida tellus faucibus. Maecenas dolor elit, pulvinar eu vehicula eu, consequat et lacus. Duis et purus ipsum. In auctor mattis ipsum id molestie. Donec risus nulla, fringilla a rhoncus vitae, semper a massa. Vivamus ullamcorper, enim sit amet consequat laoreet, tortor tortor dictum urna, ut egestas urna ipsum nec libero. Nulla justo leo, molestie vel tempor nec, egestas at massa. Aenean pulvinar, felis porttitor iaculis pulvinar, odio orci sodales odio, ac pulvinar felis quam sit.
                        </div>
                    </section>
                </div>
            </div>

        </div>



        <?php include 'include/inc-footer.php'; ?>
    </div>
    <?php include 'include/inc-script.php'; ?>


</body>

</html>