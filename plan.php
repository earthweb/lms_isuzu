<!DOCTYPE html>
<html>

<head>
    <title>Course Plan</title>
    <?php include 'include/inc-head.php'; ?>
    <style>
        .table-plan-container {
            overflow-x: auto;
        }

        .table-plan {
            border-collapse: collapse;
            display: grid;
            width: 100%;
            grid-template-columns: minmax(400px, 3fr) repeat(12, minmax(70px, 1fr));
            grid-template-rows: 60px;
        }

        .cell {
            border: 1px solid #dddddd;
            padding: 15px 5px;
            text-align: center;
        }

        .cell:nth-of-type(13n + 1) {
            grid-column: 1/1;
            text-align: start;
            padding: 15px 20px;
        }

        .cell:nth-of-type(13n + 2) {
            grid-column: 2/2;
        }

        .cell:nth-of-type(13n + 3) {
            grid-column: 3/3;
        }

        .cell:nth-of-type(13n + 4) {
            grid-column: 4/4;
        }

        .cell:nth-of-type(13n + 5) {
            grid-column: 5/5;
        }

        .cell:nth-of-type(13n + 6) {
            grid-column: 6/6;
        }

        .cell:nth-of-type(13n + 7) {
            grid-column: 7/7;
        }

        .cell:nth-of-type(13n + 8) {
            grid-column: 8/8;
        }

        .cell:nth-of-type(13n + 9) {
            grid-column: 9/9;
        }

        .cell:nth-of-type(13n + 10) {
            grid-column: 10/10;
        }

        .cell:nth-of-type(13n + 11) {
            grid-column: 11/11;
        }

        .cell:nth-of-type(13n + 12) {
            grid-column: 12/12;
        }

        .cell:nth-of-type(13n + 13) {
            grid-column: 13/13;
        }

        .th {
            font-weight: bold;
            border-top: 2px solid #dddddd;
            border-bottom: 2px solid #dddddd;
            background-color: #FBFBFB;
        }

        .event {
            padding: 0 10px;
            position: relative;
            align-self: center;
            color: #ffffff;

        }
    </style>
</head>

<body data-plugin-page-transition>

    <div class="body">
        <?php include 'include/inc-header.php'; ?>

        <div role="main" class="main">

            <section class="page-header page-header-modern mb-0">
                <div class="container-xxl">
                    <div class="row">
                        <div class="col-sm-6 align-self-center text-start">
                            <h1 class="text-dark">Course Plan</h1>
                        </div>
                        <div class="col-sm-6 align-self-center text-start text-sm-end">
                            <ul class="breadcrumb d-block">
                                <li><a href="#">Home</a></li>
                                <li class="active">Course Plan</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>

            <div class="container-xxl">
                <div class="py-5">
                    <h4 class="topic">2021<span> : Tim Schedule of each Course</span></h4>
                    <div class=" my-4">
                        <div class="table-plan-container">
                            <div id="table-plan" class="table-plan">
                                <div class='cell th'>Course Name</div>
                                <div class='cell th'>Jan</div>
                                <div class='cell th'>Feb</div>
                                <div class='cell th'>Mar</div>
                                <div class='cell th'>Apr</div>
                                <div class='cell th'>May</div>
                                <div class='cell th'>Jun</div>
                                <div class='cell th'>Jul</div>
                                <div class='cell th'>Aug</div>
                                <div class='cell th'>Sep</div>
                                <div class='cell th'>Oct</div>
                                <div class='cell th'>Nov</div>
                                <div class='cell th'>Dec</div>
                                <div class='cell' style='grid-row:2;'>Course Name</div>
                                <div class='cell' style='grid-row:2;'></div>
                                <div class='cell' style='grid-row:2;'></div>
                                <div class='cell' style='grid-row:2;'></div>
                                <div class='cell' style='grid-row:2;'></div>
                                <div class='cell' style='grid-row:2;'></div>
                                <div class='cell' style='grid-row:2;'></div>
                                <div class='cell' style='grid-row:2;'></div>
                                <div class='cell' style='grid-row:2;'></div>
                                <div class='cell' style='grid-row:2;'></div>
                                <div class='cell' style='grid-row:2;'></div>
                                <div class='cell' style='grid-row:2;'></div>
                                <div class='cell' style='grid-row:2;'></div>
                                <div class='cell' style='grid-row:3;'>Course Name</div>
                                <div class='cell' style='grid-row:3;'></div>
                                <div class='cell' style='grid-row:3;'></div>
                                <div class='cell' style='grid-row:3;'></div>
                                <div class='cell' style='grid-row:3;'></div>
                                <div class='cell' style='grid-row:3;'></div>
                                <div class='cell' style='grid-row:3;'></div>
                                <div class='cell' style='grid-row:3;'></div>
                                <div class='cell' style='grid-row:3;'></div>
                                <div class='cell' style='grid-row:3;'></div>
                                <div class='cell' style='grid-row:3;'></div>
                                <div class='cell' style='grid-row:3;'></div>
                                <div class='cell' style='grid-row:3;'></div>
                                <div class='cell' style='grid-row:4;'>Course Name</div>
                                <div class='cell' style='grid-row:4;'></div>
                                <div class='cell' style='grid-row:4;'></div>
                                <div class='cell' style='grid-row:4;'></div>
                                <div class='cell' style='grid-row:4;'></div>
                                <div class='cell' style='grid-row:4;'></div>
                                <div class='cell' style='grid-row:4;'></div>
                                <div class='cell' style='grid-row:4;'></div>
                                <div class='cell' style='grid-row:4;'></div>
                                <div class='cell' style='grid-row:4;'></div>
                                <div class='cell' style='grid-row:4;'></div>
                                <div class='cell' style='grid-row:4;'></div>
                                <div class='cell' style='grid-row:4;'></div>
                                <div class='cell' style='grid-row:5;'>Course Name</div>
                                <div class='cell' style='grid-row:5;'></div>
                                <div class='cell' style='grid-row:5;'></div>
                                <div class='cell' style='grid-row:5;'></div>
                                <div class='cell' style='grid-row:5;'></div>
                                <div class='cell' style='grid-row:5;'></div>
                                <div class='cell' style='grid-row:5;'></div>
                                <div class='cell' style='grid-row:5;'></div>
                                <div class='cell' style='grid-row:5;'></div>
                                <div class='cell' style='grid-row:5;'></div>
                                <div class='cell' style='grid-row:5;'></div>
                                <div class='cell' style='grid-row:5;'></div>
                                <div class='cell' style='grid-row:5;'></div>
                                <div class='cell' style='grid-row:6;'>Course Name</div>
                                <div class='cell' style='grid-row:6;'></div>
                                <div class='cell' style='grid-row:6;'></div>
                                <div class='cell' style='grid-row:6;'></div>
                                <div class='cell' style='grid-row:6;'></div>
                                <div class='cell' style='grid-row:6;'></div>
                                <div class='cell' style='grid-row:6;'></div>
                                <div class='cell' style='grid-row:6;'></div>
                                <div class='cell' style='grid-row:6;'></div>
                                <div class='cell' style='grid-row:6;'></div>
                                <div class='cell' style='grid-row:6;'></div>
                                <div class='cell' style='grid-row:6;'></div>
                                <div class='cell' style='grid-row:6;'></div>
                                <div class='cell' style='grid-row:7;'>Course Name</div>
                                <div class='cell' style='grid-row:7;'></div>
                                <div class='cell' style='grid-row:7;'></div>
                                <div class='cell' style='grid-row:7;'></div>
                                <div class='cell' style='grid-row:7;'></div>
                                <div class='cell' style='grid-row:7;'></div>
                                <div class='cell' style='grid-row:7;'></div>
                                <div class='cell' style='grid-row:7;'></div>
                                <div class='cell' style='grid-row:7;'></div>
                                <div class='cell' style='grid-row:7;'></div>
                                <div class='cell' style='grid-row:7;'></div>
                                <div class='cell' style='grid-row:7;'></div>
                                <div class='cell' style='grid-row:7;'></div>
                                <section class='event row-plan2'> 4 Jan - 28 Feb </section>
                                <section class='event row-plan3'> 1 Mar - 31 Apr </section>
                                <section class='event row-plan4'> 01 May 2564 </section>
                                <section class='event row-plan5'> 01 Sep - 31 Dec </section>
                                <section class='event row-plan6'> 01 Jan - 31 Dec </section>
                                <section class='event row-plan7'> 01 Jan - 31 Sep </section>

                            </div>

                            <div class="form-group col">
                                <div  class="btn-plan1 text-4 btn-plan py-2 my-4">Not Started</div>
                                <div  class="btn-plan2 text-4 btn-plan py-2 my-4">In Progress</div>
                                <div  class="btn-plan3 text-4 btn-plan py-2 my-4">Passed</div>
                                <div  class="btn-plan4 text-4 btn-plan py-2 my-4">Expired</div>
                                <!-- <button type="submit" class="btn btn-success text-4 btn-plan py-2 my-4">Passed</button>
                                <button type="submit" class="btn btn-danger text-4 py-2 my-4 btn-plan">Expired</button> -->
                            </div>


                        </div>
                    </div>
                </div>
            </div>
            <?php include 'include/inc-footer.php'; ?>
        </div>
        <?php include 'include/inc-script.php'; ?>
    </div>
</body>

</html>