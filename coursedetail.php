<!DOCTYPE html>
<html>

<head>
    <title>Course Name</title>
    <?php include 'include/inc-head.php'; ?>
</head>

<body data-plugin-page-transition>

    <div class="body">
        <?php include 'include/inc-header.php'; ?>

        <div role="main" class="main">

            <section class="page-header page-header-modern mb-0">
                <div class="container-xxl">
                    <div class="row">
                        <div class="col-md-6 align-self-center text-start">
                            <h1 class="text-dark">Course Name</h1>
                        </div>
                        <div class="col-md-6 align-self-center text-start text-md-end">
                            <ul class="breadcrumb d-block">
                                <li><a href="#">Home</a></li>
                                <li><a href="#">Course</a></li>
                                <li class="active">Lessons 1</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>

            <div class="container-xxl">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="course-menu">
                            <div class="row mb-4">
                                <div class="col">
                                    <div class="card">
                                        <img class="card-img-top rounded" src="img/1-main/thumbnail.png" alt="">
                                        <div class="card-body">
                                            <div class="course-name">
                                                <p class="card-title text-4 text-dark">Course Name</p>
                                            </div>
                                            <div class="padding course-detail">
                                                <div class="progress progress-sm progress-border-radius border my-2">
                                                    <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
                                                </div>
                                                <p class="card-text text-dark mb-2">60 % Progress</p>
                                                <p class="card-text">Course Instructor : <span class="text-dark">Mr.Sanit</span></p>
                                                <p class="card-text">Course Approver : <span class="text-dark">Mr.Denail</span></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row mb-4">
                                <div class="col">
                                    <div class="card course-detail">
                                        <div class="card-body row p-2">
                                            <div class="col border-end padding">
                                                <p class="mb-1 detail">Time</p>
                                                <img class="mx-auto d-block mt-3 mb-2" src=".\img\1-main\clock-icon.png">
                                                <p class="text-center detail-value">2 Hr</p>
                                            </div>
                                            <div class="col padding">
                                                <p class="mb-1 detail">Lessons</p>
                                                <img class="mx-auto d-block mt-3 mb-2" src=".\img\1-main\book-icon.png">
                                                <p class="text-center detail-value">4 Lessons</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row mb-4">
                                <div class="col">
                                    <div class="card course-detail">
                                        <div class="card-body row p-2">
                                            <div class="col border-end padding">
                                                <p class="mb-1 detail">Status</p>
                                                <img class="mx-auto d-block my-4" src=".\img\1-main\certificate-icon-mute.png">
                                                <p class="text-center detail-value mb-3">Failed</p>
                                            </div>
                                            <div class="col padding">
                                                <p class="mb-1 detail">Course Evaluation</p>
                                                <img class="mx-auto d-block my-4" src=".\img\1-main\questionnaire-icon.png">
                                                <p class="text-center"><a href="./questionnaire.php" class="text-decoration-none btn-main detail-value">Click</a></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-8">
                        <div class="row">
                            <div class="col-12">
                                <div class="course-content">
                                    <div class="tabs tabs-bottom tabs-center tabs-simple">
                                        <ul class="nav nav-tabs justify-content-start">
                                            <li class="nav-item">
                                                <a class="nav-link active" href="#tabsNavigationSimple1" data-bs-toggle="tab">Course Details</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#tabsNavigationSimple2" data-bs-toggle="tab">Course List</a>
                                            </li>
                                        </ul>
                                        <div class="tab-content mt-4">
                                            <div class="tab-pane active" id="tabsNavigationSimple1">
                                                <div>
                                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book</p>
                                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book</p>
                                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book</p>
                                                    <p>The course is suitable for?</p>
                                                    <ul>
                                                        <li>Lorem Ipsum is simply dummy text</li>
                                                        <li>Lorem Ipsum is simply dummy text</li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="tabsNavigationSimple2">
                                                <div class="row">
                                                    <div class="col-12 course-section mb-3">
                                                        <div class="section-title">
                                                            <span>Lesson 1 Course Name</span>
                                                            <div class="float-end d-flex"><div class="btn-main rounded-3 course-btn-documents text-center"><img src="./img/1-main/download-icon.png"> Documents</div></div>
                                                        </div>
                                                        <ul class="section-list">
                                                            <li>
                                                                <a href="">
                                                                    <span class="staus-learn"><img src=".\img\1-main\check-icon.png"></span>Pre-Test
                                                                    <div class="float-end d-flex"><span class="mx-2">Score</span> <div class="border border-2 rounded-3 text-center course-btn">8/15</div></div>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href=""><span class="staus-learn"><img src=".\img\1-main\halfcheck-icon.png"></span>Video Lesson 1
                                                                    <div class="float-end d-flex"><span class="mx-2"><i class="far fa-clock"></i> 30 mins </span><div class="btn-main rounded-3 course-btn text-center">Watch</div></div>
                                                                </a>
                                                            </li>

                                                            <li>
                                                                <a href=""><span class="staus-learn"><img src=".\img\1-main\uncheck-icon.png"></span>Post-Test
                                                                    <div class="float-end d-flex"><div class="btn-main rounded-3 course-btn text-center">Click</div></div>
                                                                </a>
                                                            </li>

                                                        </ul>
                                                    </div>
                                                    <div class="col-12 course-section mb-3">
                                                        <div class="section-title">
                                                            <span>Lesson 2 Course Name</span>
                                                            <div class="float-end d-flex"><div class="btn-main rounded-3 course-btn-no-documents text-center">No Documents</div></div>
                                                        </div>
                                                        <ul class="section-list">
                                                            <li>
                                                                <a href="">
                                                                    <span class="staus-learn"><img src=".\img\1-main\uncheck-icon.png"></span>Pre-Test
                                                                    <div class="float-end d-flex"> <div class="text-center course-btn"><img src="./img/1-main/lock-icon.png"></div></div>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href=""><span class="staus-learn"><img src=".\img\1-main\uncheck-icon.png"></span>Video Lesson 2
                                                                    <div class="float-end d-flex"><span class="mx-2"><i class="far fa-clock"></i> 30 mins </span><div class="course-btn text-center"><img src="./img/1-main/lock-icon.png"></div></div>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href=""><span class="staus-learn"><img src=".\img\1-main\uncheck-icon.png"></span>Post-Test
                                                                    <div class="float-end d-flex"><div class="course-btn text-center"><img src="./img/1-main/lock-icon.png"></div></div>
                                                                </a>
                                                            </li>

                                                        </ul>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>

        </div>
    </div>

    <?php include 'include/inc-footermain.php'; ?>
    </div>
    <?php include 'include/inc-script.php'; ?>


</body>

</html>