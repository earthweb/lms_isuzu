<!DOCTYPE html>
<html>

<head>
    <title>Contact Us</title>
    <?php include 'include/inc-head.php'; ?>
</head>

<body data-plugin-page-transition>

    <div class="body">
        <?php include 'include/inc-header.php'; ?>

        <div role="main" class="main">

            <section class="page-header page-header-modern mb-0">
                <div class="container-xxl">
                    <div class="row">
                        <div class="col-sm-6 align-self-center text-start">
                            <h1 class="text-dark">Contact Us</h1>
                        </div>
                        <div class="col-sm-6 align-self-center text-start text-sm-end">
                            <ul class="breadcrumb d-block">
                                <li><a href="#">Home</a></li>
                                <li class="active">Contact Us</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>

            <div class="container content">
                <div class="row justify-content-center gy-4">
                    <div class="col-lg-6 pt-3">
                        <img src="./img/1-main/Company.png" class="img-fluid mt-3" alt="Company">
                    </div>
                    <div class="col-lg-6">
                        <h4 class="contact-title"><span>Admin</span></h4>
                        <div class="row pt-2 gy-3">
                            <div class="col-sm-6">
                                <div class="card border p-2">
                                    <img src="./img/1-main/avatar-1.png" class="img-fluid">
                                    <div class="contect-detial px-3 pt-4 pb-2">
                                        <p>Mr. FirstName LastName</p>
                                        <p>Position : Admin staff</p>
                                        <p>Phone Number : 0-2966-2222</p>
                                        <p>Email : info@isuzu-tis.com</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="card border p-2">
                                    <img src="./img/1-main/avatar-2.png" class="img-fluid">
                                    <div class="contect-detial px-3 pt-4 pb-2">
                                        <p>Mr. FirstName LastName</p>
                                        <p>Position : Admin staff</p>
                                        <p>Phone Number : 0-2966-2111</p>
                                        <p>Email : info@isuzu-tis.com</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>


        </div>



        <?php include 'include/inc-footer.php'; ?>
    </div>
    <?php include 'include/inc-script.php'; ?>


</body>

</html>