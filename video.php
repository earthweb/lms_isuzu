<!DOCTYPE html>
<html>

<head>
    <title>Recommended Video</title>
    <?php include 'include/inc-head.php'; ?>
</head>

<body data-plugin-page-transition>

    <div class="body">
        <?php include 'include/inc-header.php'; ?>

        <div role="main" class="main">

            <section class="page-header page-header-modern mb-0">
                <div class="container-xxl">
                    <div class="row">
                        <div class="col-md-6 align-self-center text-start">
                            <h1 class="text-dark">Recommended Video</h1>
                        </div>
                        <div class="col-md-6 align-self-center text-start text-md-end">
                            <ul class="breadcrumb d-block">
                                <li><a href="#">Home</a></li>
                                <li class="active">Recommended Video</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>

            <div class="container-sm content">
                <div class="row g-5">

                    <div class="col-md-6 col-lg-4 mb-4">
                        <article class="card border-2 rounded p-3 p-md-4">
                            <div class="ratio ratio-4x3">
                                <video id="video1" class="rounded" width="100%" height="100%" muted loop preload="metadata" poster="./video/presentation.jpg">
                                    <source src="./video/movie.mp4" type="video/mp4">
                                    <source src="./video/movie.ogv" type="video/ogv">
                                </video>
                                <a href="#" class="position-absolute top-50pct left-50pct transform3dxy-n50 bg-light rounded-circle d-flex align-items-center justify-content-center text-decoration-none bg-color-hover-primary text-color-hover-light play-button-lg pulseAnim pulseAnimAnimated" data-trigger-play-video="#video1" data-trigger-play-video-remove="yes">
                                    <i class="fas fa-play text-5"></i>
                                </a>
                            </div>
                            <p class="text-center text-4 text-dark mt-4 mb-2">Trial Course</p>
                        </article>
                    </div>
                    <div class="col-md-6 col-lg-4 mb-4">
                        <article class="card border-2 rounded p-3 p-md-4">
                            <div class="ratio ratio-4x3">
                                <video id="video2" class="rounded" width="100%" height="100%" muted loop preload="metadata" poster="./video/presentation.jpg">
                                    <source src="./video/movie.mp4" type="video/mp4">
                                    <source src="./video/movie.ogv" type="video/ogv">
                                </video>
                                <a href="#" class="position-absolute top-50pct left-50pct transform3dxy-n50 bg-light rounded-circle d-flex align-items-center justify-content-center text-decoration-none bg-color-hover-primary text-color-hover-light play-button-lg pulseAnim pulseAnimAnimated" data-trigger-play-video="#video2" data-trigger-play-video-remove="yes">
                                    <i class="fas fa-play text-5"></i>
                                </a>
                            </div>
                            <p class="text-center text-4 text-dark mt-4 mb-2">Trial Course</p>
                        </article>
                    </div>
                    <div class="col-md-6 col-lg-4 mb-4">
                        <article class="card border-2 rounded p-3 p-md-4">
                            <div class="ratio ratio-4x3">
                                <video id="video3" class="rounded" width="100%" height="100%" muted loop preload="metadata" poster="./video/presentation.jpg">
                                    <source src="./video/movie.mp4" type="video/mp4">
                                    <source src="./video/movie.ogv" type="video/ogv">
                                </video>
                                <a href="#" class="position-absolute top-50pct left-50pct transform3dxy-n50 bg-light rounded-circle d-flex align-items-center justify-content-center text-decoration-none bg-color-hover-primary text-color-hover-light play-button-lg pulseAnim pulseAnimAnimated" data-trigger-play-video="#video3" data-trigger-play-video-remove="yes">
                                    <i class="fas fa-play text-5"></i>
                                </a>
                            </div>
                            <p class="text-center text-4 text-dark mt-4 mb-2">Trial Course</p>
                        </article>
                    </div>

                </div>
            </div>

        </div>

        <?php include 'include/inc-footer.php'; ?>
    </div>
    <?php include 'include/inc-script.php'; ?>


</body>

</html>