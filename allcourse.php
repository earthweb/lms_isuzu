<!DOCTYPE html>
<html>

<head>
    <title>Course</title>
    <?php include 'include/inc-head.php'; ?>
</head>

<body data-plugin-page-transition>

    <div class="body">
        <?php include 'include/inc-header.php'; ?>

        <div role="main" class="main">

            <section class="page-header page-header-modern mb-0">
                <div class="container-xxl">
                    <div class="row">
                        <div class="col-sm-6 align-self-center text-start">
                            <h1 class="text-dark">Course</h1>
                        </div>
                        <div class="col-sm-6 align-self-center text-start text-sm-end">
                            <ul class="breadcrumb d-block">
                                <li><a href="#">Home</a></li>
                                <li class="active">All Course</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>

            <div class="container-sm content">
                <div class="row">
                    <div class="col-lg-2 bg-filter">
                        <aside class="sidebar">
                            <h5 class="text-4 text-dark my-1 py-2 text-capitalize">Course Category</h5>
                            <div class="toggle toggle-minimal toggle-primary" data-plugin-toggle data-plugin-options="{ 'isAccordion': true }">
                                <section class="toggle active">
                                    <a class="toggle-title text-4">Type 1</a>
                                    <div class="toggle-content">
                                        <p><a href="#">Course 1</a></p>
                                        <p><a href="#">Course 2</a></p>
                                        <p><a href="#">Course 3</a></p>
                                    </div>
                                </section>
                                <section class="toggle">
                                    <a class="toggle-title text-4">Type 2</a>
                                    <div class="toggle-content">
                                        <p><a href="#">Course 1</a></p>
                                        <p><a href="#">Course 2</a></p>
                                        <p><a href="#">Course 3</a></p>
                                    </div>
                                </section>
                                <section class="toggle">
                                    <a class="toggle-title text-4">Type 3</a>
                                    <div class="toggle-content">
                                        <p><a href="#">Course 1</a></p>
                                        <p><a href="#">Course 2</a></p>
                                        <p><a href="#">Course 3</a></p>
                                    </div>
                                </section>
                                <section class="toggle">
                                    <a class="toggle-title text-4">Type 4</a>
                                    <div class="toggle-content">
                                        <p><a href="#">Course 1</a></p>
                                        <p><a href="#">Course 2</a></p>
                                        <p><a href="#">Course 3</a></p>
                                    </div>
                                </section>
                            </div>
                        </aside>
                    </div>

                    <div class="col-lg-10 section-allcourse">

                        <div class="row justify-content-end">
                            <select class="form-select mb-4 w-25" aria-label="Default select example">
                                <option selected>Sort By:</option>
                                <option value="1">A - Z</option>
                                <option value="2">Z - A</option>
                            </select>
                        </div>

                        <div class="row g-5">
                            <?php for ($i = 0; $i < 8; $i++) { ?>
                                <div class="col-md-4 col-lg-3">
                                    <div class="card card-course">
                                        <a href="#">
                                            <img class="card-img-top" src="img/1-main/allcourse-pic.png">
                                        </a>
                                        <div class="card-body">
                                            <p class="card-text mb-2 text-category"><i class="far fa-play-circle"></i> Category</p>
                                            <h6 class="card-title mb-2 text-4 text-main "><a href="#">Course Name</a></h6>
                                            <p class="card-text mb-2 text-category"><i class="far fa-calendar-alt"></i> Length :
                                                <span style="color:#000000">18.06.64-18.07.64</span>
                                            </p>
                                            <hr class="mb-2">
                                            <span class="card-text "><i class="icon-clock"></i> 1 hr 30 min</span>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php include 'include/inc-footermain.php'; ?>
    </div>
    <?php include 'include/inc-script.php'; ?>


</body>

</html>