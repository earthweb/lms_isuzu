<!DOCTYPE html>
<html>

<head>
    <title>Course Status</title>
    <?php include 'include/inc-head.php'; ?>
</head>

<body data-plugin-page-transition>

    <div class="body">
        <?php include 'include/inc-header.php'; ?>

        <div role="main" class="main">

            <section class="page-header page-header-modern mb-0">
                <div class="container-xxl">
                    <div class="row">
                        <div class="col-sm-6 align-self-center text-start">
                            <h1 class="text-dark">Course Status</h1>
                        </div>
                        <div class="col-sm-6 align-self-center text-start text-sm-end">
                            <ul class="breadcrumb d-block">
                                <li><a href="#">Home</a></li>
                                <li class="active">Course Status</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>

            <div class="container-xxl">
                <div class="row g-2 ">

                    <div class="sidebar col-md-3 col-lg-2 ">
                        <div class="container-fluid">
                            <p class="my-0"><a href="./profile.php">Personal Information</a></p>
                            <hr class="my-2">
                            <p class="my-0">Course Status</p>
                            <hr class="my-2">
                        </div>
                    </div>

                    <div class="content col col-md-9 col-lg-10 ">
                        <div class="container pb-5">
                            <div class="row g-5">

                                <div class="col-sm-6 col-lg-4">
                                    <div class="card card-course">
                                        <a href="#"><img class="card-img-top" src="img/1-main/course-image.png"></a>
                                        <div class="card-body" style="padding:10px;">
                                            <h6 class="card-title mb-2 text-4 text-main "><a href="#">Course Name</a></h6>
                                            <div class="progress progress-sm progress-border-radius mt-4 mb-2">
                                                <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                                                </div>
                                            </div>
                                            <p class="text-dark mb-2">0 % Complete </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-lg-4">
                                    <div class="card card-course">
                                        <a href="#"><img class="card-img-top" src="img/1-main/course-image.png"></a>
                                        <div class="card-body" style="padding:10px;">
                                            <h6 class="card-title mb-2 text-4 text-main "><a href="#">Course Name</a></h6>
                                            <div class="progress progress-sm progress-border-radius mt-4 mb-2">
                                                <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                                                </div>
                                            </div>
                                            <p class="text-dark mb-2">0 % Complete </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-lg-4">
                                    <div class="card card-course">
                                        <a href="#"><img class="card-img-top" src="img/1-main/course-image.png"></a>
                                        <div class="card-body" style="padding:10px;">
                                            <h6 class="card-title mb-2 text-4 text-main "><a href="#">Course Name</a></h6>
                                            <div class="progress progress-sm progress-border-radius mt-4 mb-2">
                                                <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                                                </div>
                                            </div>
                                            <p class="text-dark mb-2">0 % Complete </p>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php include 'include/inc-footer.php'; ?>
    </div>
    <?php include 'include/inc-script.php'; ?>
</body>

</html>