<!DOCTYPE html>
<html>

<head>
    <title>Post-test</title>
    <?php include 'include/inc-head.php'; ?>
</head>

<body data-plugin-page-transition>

    <div class="body">
        <?php include 'include/inc-header.php'; ?>

        <div role="main" class="main">

            <section class="page-header page-header-modern mb-0">
                <div class="container-xxl">
                    <div class="row">
                        <div class="col-sm-6 align-self-center text-start">
                            <h1 class="text-dark">Post-test</h1>
                        </div>
                        <div class="col-sm-6 align-self-center text-start text-sm-end">
                            <ul class="breadcrumb d-block">
                                <li><a href="#">Home</a></li>
                                <li><a href="#">All course</a></li>
                                <li class="active">Lesson 1</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>

            <section class="section-main">
                <div class="container py-5">
                    <div class="row justify-content-center">
                        <div class="col col-md-7 col-lg-5">
                            <h4>Post-Test Lesson 1</h4>
                            <div class="card mb-4">
                                <div class="card-body row p-4">
                                    <div class="col">
                                        <p class="mb-1 text-3">Time</p>
                                        <h6 class="text-3"><img src=".\img\1-main\clock-icon-sm.png"> 30 min</h6>
                                    </div>
                                    <hr class="mt-4">
                                    <div class="col">
                                        <p class="mb-1 text-3">Number of Question</p>
                                        <h6 class="text-3">15 Questions</h6>
                                    </div>
                                    <hr class="mt-4">
                                    <div class="col">
                                        <p class="mb-1 text-3">Passing Score</p>
                                        <h6 class="text-3">50 %</h6>
                                    </div>
                                </div>
                            </div>
                            <div class="text-center">
                                <a href="./pretest-2.php" class="btn btn-main text-decoration-none px-5">
                                    start
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <?php include 'include/inc-footermain.php'; ?>
        </div>
        <?php include 'include/inc-script.php'; ?>


</body>

</html>