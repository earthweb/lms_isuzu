<!DOCTYPE html>
<html>

<head>
    <title>Course Evaluation</title>
    <?php include 'include/inc-head.php'; ?>
    <style>
        .table-questionnaire {
            min-width: 700px;
            text-align: center;
        }

        .table-questionnaire thead tr th {
            vertical-align: middle;
        }

        .table-questionnaire tbody tr td:nth-child(2) {
            text-align: left;
            width: 300px;
        }

        .table-questionnaire td,
        .table-questionnaire th {
            width: 75px;
        }

        #suggestion {
            width: 100%;
            height: 100px;
            padding: 10px;
        }
    </style>
    <link href="./vendor/icheck/skins/square/red.css" rel="stylesheet">
</head>

<body data-plugin-page-transition>

    <div class="body">
        <?php include 'include/inc-header.php'; ?>

        <div role="main" class="main">

            <section class="page-header page-header-modern mb-0">
                <div class="container-xxl">
                    <div class="row">
                        <div class="col-sm-6 align-self-center text-start">
                            <h1 class="text-dark">Course Evaluation</h1>
                        </div>
                        <div class="col-sm-6 align-self-center text-start text-sm-end">
                            <ul class="breadcrumb d-block">
                                <li><a href="#">Home</a></li>
                                <li class="active">Course Evaluation</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>

            <div class="container">
                <div class="row mt-4">
                    <h4 class="mb-0 topic"> Course Evaluation</h4>
                    <form>
                        <div class="col table-responsive my-4">
                            <table class="table table-bordered table-questionnaire">
                                <thead class="questionnaire-title">
                                    <tr>
                                        <th rowspan="2">No</th>
                                        <th rowspan="2" class="detail">Topic</th>
                                        <th colspan="5">Score</th>
                                    </tr>
                                    <tr>
                                        <th>5<br>Very Good</th>
                                        <th>4<br>Good</th>
                                        <th>3<br>Fair</th>
                                        <th>2<br>Poor</th>
                                        <th>1<br>Improve</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>Lorem ipsum dolor sit amet consectetur adipisicing elit.</td>
                                        <!-- <form> -->
                                        <td><input type="radio" name="1" value="5"></td>
                                        <td><input type="radio" name="1" value="4"></td>
                                        <td><input type="radio" name="1" value="3"></td>
                                        <td><input type="radio" name="1" value="2"></td>
                                        <td><input type="radio" name="1" value="1"></td>
                                        <!-- </form> -->
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>Lorem ipsum dolor sit amet consectetur adipisicing elit.</td>
                                        <!-- <form> -->
                                        <td><input type="radio" name="2" value="5"></td>
                                        <td><input type="radio" name="2" value="4"></td>
                                        <td><input type="radio" name="2" value="3"></td>
                                        <td><input type="radio" name="2" value="2"></td>
                                        <td><input type="radio" name="2" value="1"></td>
                                        <!-- </form> -->
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>Lorem ipsum dolor sit amet consectetur adipisicing elit.</td>
                                        <!-- <form> -->
                                        <td><input type="radio" name="3" value="5"></td>
                                        <td><input type="radio" name="3" value="4"></td>
                                        <td><input type="radio" name="3" value="3"></td>
                                        <td><input type="radio" name="3" value="2"></td>
                                        <td><input type="radio" name="3" value="1"></td>
                                        <!-- </form> -->
                                    </tr>
                                    <tr>
                                        <td>4</td>
                                        <td>Lorem ipsum dolor sit amet consectetur adipisicing elit.</td>
                                        <!-- <form> -->
                                        <td><input type="radio" name="4" value="5"></td>
                                        <td><input type="radio" name="4" value="4"></td>
                                        <td><input type="radio" name="4" value="3"></td>
                                        <td><input type="radio" name="4" value="2"></td>
                                        <td><input type="radio" name="4" value="1"></td>
                                        <!-- </form> -->
                                    </tr>
                                    <tr>
                                        <td>5</td>
                                        <td>Lorem ipsum dolor sit amet consectetur adipisicing elit.</td>
                                        <!-- <form> -->
                                        <td><input type="radio" name="5" value="5"></td>
                                        <td><input type="radio" name="5" value="4"></td>
                                        <td><input type="radio" name="5" value="3"></td>
                                        <td><input type="radio" name="5" value="2"></td>
                                        <td><input type="radio" name="5" value="1"></td>
                                        <!-- </form> -->
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col card mb-5">
                            <div class="card-body">
                                <h6>Comments or Suggestions</h6>
                                <textarea class="questionnaire-comment" id="suggestion" placeholder="Write your Comment"></textarea>
                            </div>
                        </div>
                        <div class="text-center mb-5">
                            <a href="#" class="btn btn-main text-decoration-none px-5">
                                Send
                            </a>
                        </div>

                    </form>
                </div>
            </div>
        </div>

        <?php include 'include/inc-footer.php'; ?>
    </div>
    <?php include 'include/inc-script.php'; ?>
    <script src="./vendor/icheck/icheck.js"></script>
    <script>
        $(document).ready(function() {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square',
                radioClass: 'iradio_square'
            });
        });
    </script>
    <script>
        $(document).ready(function() {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-red',
                radioClass: 'iradio_square-red'
            });
        });
    </script>

</body>

</html>