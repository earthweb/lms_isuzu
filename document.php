<!DOCTYPE html>
<html>

<head>
    <title>Download Files</title>
    <?php include 'include/inc-head.php'; ?>
</head>

<body data-plugin-page-transition>

    <div class="body">
        <?php include 'include/inc-header.php'; ?>

        <div role="main" class="main">

            <section class="page-header page-header-modern mb-0">
                <div class="container-xxl">
                    <div class="row">
                        <div class="col-sm-6 align-self-center text-start">
                            <h1 class="text-dark">Download Files</h1>
                        </div>
                        <div class="col-sm-6 align-self-center text-start text-sm-end">
                            <ul class="breadcrumb d-block">
                                <li><a href="#">Home</a></li>
                                <li class="active">Download Files</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>

            <div class="container content">
                <div class="row justify-content-between align-items-end mb-3">
                    <div class="col-7 col-md-8 col-lg-9">
                        <h4 class="topic mb-0"> Latest Document</h4>
                    </div>
                    <div class="col-5 col-md-4 col-lg-3">
                        <input class="form-control text-3" type="text" style="width: 100%;" placeholder="Type a search term">
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col table-responsive">
                        <table class="table table-document">
                            <thead>
                                <tr>
                                    <td>No.</td>
                                    <td>Document Name</td>
                                    <td>Announced Date</td>
                                    <td></td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>Lorem ipsum dolor sit amet consectetur adipisicing elit.</td>
                                    <td>11/05/2564</td>
                                    <td>
                                        <button class="btn btn-download text-white" type="button">Download</button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>Lorem ipsum dolor sit amet consectetur adipisicing elit. Similique, necessitatibus odio consequatur, nesciunt doloribus aperiam qui est sint inventore suscipit nihil. Impedit numquam saepe mollitia minima animi vitae commodi beatae!</td>
                                    <td>22/05/2564</td>
                                    <td>
                                        <button class="btn btn-download text-white" type="button">Download</button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>

        <?php include 'include/inc-footer.php'; ?>
    </div>
    <?php include 'include/inc-script.php'; ?>


</body>

</html>