<!DOCTYPE html>
<html>

<head>
    <title>หน้าแรก</title>
    <?php include 'include/inc-head.php'; ?>

    <style>
        #branding {
            background-image: url(./img/1-main/password-bg.png);
            background-repeat: no-repeat;
            background-size: cover;
            background-position: left bottom;
        }
    </style>

</head>

<body data-plugin-page-transition>

    <div class="body">
        <div role="main" class="main">

            <div class="header-logo mt-0 mb-0">
                <a href="index.php">
                    <img alt="" class="position-fixed d-none d-md-block" style="left:70px;" height="150" src="./img/1-main/logo.png">
                </a>
            </div>

            <div class="row vh-100">
                <div id="branding" class="col-md-7 d-none d-md-block"></div>
                <div class="col col-md-5 p-5 float-end">
                    <div class="login section">
                        <a class="text-decoration-none" href="index.php"><i class="fas fa-chevron-left text-1 me-1"></i>กลับหน้าแรก</a>
                        <h3 class="mt-5 pt-5">ตั้งค่ารหัสผ่านใหม่</h3>
                        <form action="/" id="frmSignIn" method="post" class="needs-validation">
                            <div class="form-group col">
                                <label class="form-label text-color-dark text-3">รหัสผ่านใหม่<span class="text-color-danger">*</span></label>
                                <input type="password" value="" class="form-control form-control-lg" required>
                            </div>
                            <div class="form-group col">
                                <label class="form-label text-color-dark text-3">ยืนยันรหัสผ่านใหม่<span class="text-color-danger">*</span></label>
                                <input type="password" value="" class="form-control form-control-lg" required>
                            </div>
                            <div class="row">
                                <div class="form-group col">
                                    <button type="submit" class="btn btn-main w-100 text-4 py-2 my-4" data-loading-text="Loading...">ยืนยัน</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>

    </div>
    </div>

    <?php include 'include/inc-script.php'; ?>
</body>

</html>