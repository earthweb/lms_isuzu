<!DOCTYPE html>
<html>

<head>
    <title>Home</title>
    <?php include 'include/inc-head.php'; ?>
</head>

<body data-plugin-page-transition>

    <div class="body">
        <?php include 'include/inc-header.php'; ?>

        <div role="main" class="main">

            <section class="banner-index">
                <div class="owl-carousel owl-theme nav-inside nav-style-1 nav-light" data-plugin-options="{'items': 1, 'margin': 10, 'loop': false, 'nav': true, 'dots': false}">
                    <div>
                        <div class="img-thumbnail border-0 p-0 d-block">
                            <img class="img-fluid border-radius-0" src="img/1-main/main-bg-4.png" alt="">
                            <!-- <lottie-player src="img/1-main/animation/banner-animation.json" loop autoplay background="transparent" speed="1" style="width: 100%; height: auto;"></lottie-player> -->
                            <div class="elerning-text">
                                <h5 class="elerning-text-size1">e-Learning</h5><br><br>
                                <h5 class="elerning-text-size2">ระบบเรียนรู้ออนไลน์</h5>
                            </div>

                        </div>
                    </div>

                    <div>
                        <div class="img-thumbnail border-0 p-0 d-block">
                            <img class="img-fluid border-radius-0" src="img/1-main/main-bg-4.png" alt="">
                        </div>
                    </div>
                </div>
            </section>

            <section class="menu-index">
                <div class="featured-boxes featured-boxes-style-2 menu-elearning">
                    <div class="container">
                        <div class="owl-carousel owl-theme nav-inside nav-style-1 nav-light" data-plugin-options="{'items': 5, 'margin': 10, 'loop': false, 'nav': true, 'dots': false}">
                            <di>
                                <div class="featured-box featured-box-one  featured-box-effect-4">
                                    <div class="box-content">
                                        <a href="#">
                                            <div class="icon-featured"><img src="img\1-main\online-class.svg"></div>
                                        </a>
                                        <h4 class="mb-0">Online Class</h4>
                                    </div>
                                </div>
                            </di>

                            <di>
                                <div class="featured-box featured-box-two featured-box-effect-4">
                                    <div class="box-content">
                                        <a href="#">
                                            <div class="icon-featured"><img src="img\1-main\E-Library.svg"></div>
                                        </a>
                                        <h4 class="mb-0">E-Library</h4>
                                    </div>
                                </div>
                            </di>

                            <div>
                                <div class="featured-box featured-box-three featured-box-effect-4">
                                    <div class="box-content">
                                        <a href="#">
                                            <div class="icon-featured"><img src="img\1-main\Download Files.svg"></div>
                                        </a>
                                        <h4 class="mb-0">Download Files</h4>
                                    </div>
                                </div>
                            </div>

                            <di>
                                <div class="featured-box featured-box-four featured-box-effect-4">
                                    <div class="box-content">
                                        <a href="#">
                                            <div class="icon-featured"><img src="img\1-main\Course Plan.svg"></div>
                                        </a>
                                        <h4 class="mb-0">Course Plan</h4>
                                    </div>
                                </div>
                            </di>

                            <div>
                                <div class="featured-box featured-box-five featured-box-effect-4">
                                    <div class="box-content">
                                        <a href="#">
                                            <div class="icon-featured "><img src="img\1-main\Course Status.svg"></div>
                                        </a>
                                        <h4 class="mb-0">Course Status</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="activity-index">
                <section class="course-index">

                    <div class="container pb-5">
                        <div class="row mt-5">
                            <div class="col">
                                <h3 class="title-panel our-courses"><span>Our Courses</span></h3>
                                <div class="owl-carousel owl-theme " data-plugin-options="{'items': 3, 'margin': 10, 'loop': false, 'nav': true, 'dots': false}">
                                    <?php for ($i = 0; $i < 8; $i++) { ?>
                                        <div class="py-4 px-1">
                                            <div class="card card-course1">
                                                <a href="#">
                                                    <img class="card-img-top" src="img/1-main/course-image.png">
                                                </a>
                                                <div class="card-body1">
                                                    <p class="card-text mb-2 text-warning"><i class="far fa-play-circle"></i> Course Category</p>
                                                    <h6 class="card-title mb-2 text-4 text-main1 "><a href="#">Course Name</a></h6>
                                                    <hr class="mb-2 progress">

                                                    <span class="card-text "><i class="icon-clock"></i> 1 hr 30 min.</span>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <!--
                    <div class="text-end">
                        <a href="#" class="btn-viewall">
                            <span class="h5">ดูหลักสูตรทั้งหมด <img src="img/1-main/viewall.svg"></span>
                        </a>
                    </div> -->
                    </div>
                </section>


                <div class="container">
                    <div class="row ">
                        <div class="col-lg-8 mt-2 ">
                            <h3 class="title-panel"><span>News</span></h3>

                            <div class="row ">
                                <?php for ($i = 0; $i < 3; $i++) { ?>
                                    <div class="col-4 mb-4 row-new">
                                        <article class="post">
                                            <div class="post-image">
                                                <a href="#">
                                                    <img src="img/1-main/new-img2.png" class="img-fluid img-thumbnail img-thumbnail-no-borders rounded-1 mb-2" alt="" />
                                                </a>
                                            </div>
                                            <div>
                                                <div class="blog-detail">
                                                    <p class="mb-1 text-white">Title News<br>Lorem Ipsum is simply dummy text of the printing and typesetting industry... </p>
                                                    <!--<p class="text-color-muted text-2 mb-1"><i class="icon-calendar"></i> 10 เมษายน 2564</p>-->
                                                    <button class="card read-more-btn">
                                                        <a href="#" class="read-more1  text-2">Read More</a>
                                                    </button>
                                                </div>
                                            </div>
                                        </article>
                                    </div>
                                <?php } ?>
                            </div>

                            <div class="text-end mt-3">
                                <a href="#" class="btn-viewall">
                                    <button class="view-more-btn">
                                        <span class=" read-more1">View More <img src="img/1-main/add.svg"></span>
                                    </button>
                                </a>
                            </div>
                        </div><!-- style="background-image: url('img/1-main/main-bg-2.png');" -->
                        <div class="col col-lg-4 mt-2">
                            <h3 class="title-panel"><span>Recommended Video</span></h3>
                            <div class="p-3 card card-video">
                                <article class="post">
                                    <div class="post-image">
                                        <a href="#">
                                            <video class="w-100" controls>
                                                <source src="video/deves-video.mp4" type="video/mp4">
                                                Your browser does not support the video tag.
                                            </video>
                                            <!-- <img src="img/1-main/thumbnail.png" class="img-fluid img-thumbnail img-thumbnail-no-borders rounded-0 mb-2" alt="" /> -->
                                        </a>
                                    </div>
                                    <div class="text-center">
                                        <h4><a href="#" class="text-decoration-none text-course">Trial Course</a></h4>
                                    </div>
                                </article>
                            </div>
                            <div class="text-end mt-3">
                                <a href="#" class="btn-viewall">
                                    <button class="view-more-btn">
                                        <span class="read-more1">View More <img src="img/1-main/add.svg"></span>
                                    </button>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        </div>
        <?php include 'include/inc-footer.php'; ?>
    </div>
    <?php include 'include/inc-script.php'; ?>


</body>

</html>