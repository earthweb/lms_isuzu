<!DOCTYPE html>
<html>

<head>
    <title>News</title>
    <?php include 'include/inc-head.php'; ?>
</head>

<body data-plugin-page-transition>

    <div class="body">
        <?php include 'include/inc-header.php'; ?>

        <div role="main" class="main">

            <section class="page-header page-header-modern mb-0">
                <div class="container-xxl">
                    <div class="row">
                        <div class="col-sm-6 align-self-center text-start">
                            <h1 class="text-dark">News</h1>
                        </div>
                        <div class="col-sm-6 align-self-center text-start text-sm-end">
                            <ul class="breadcrumb d-block">
                                <li><a href="#">Home</a></li>
                                <li class="active">News</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>

            <div class="container-sm content">
                <div class="row">
                    <div class="col-sm-6 col-lg-3 mb-4">
                        <div class="card">
                            <a href="#" style="text-decoration: none">
                                <img class="card-img-top" height="200px" src="img/projects/project-2.jpg" alt="Card Image">
                            </a>
                            <div class="card-body" style="padding:10px;">
                                <a href="#" style="text-decoration: none">
                                    <h6 class="card-title mb-2 text-4 ">News Event Name</h6>
                                </a>
                                <div class="mb-3"><img src=".\img\1-main\calendar-icon.png"><span class="text-2"> 10 April 2021</span></div>
                                <a href="#" class="read-more text-2 float-end mt-3" style="text-decoration: none">Read More<i class="fas fa-chevron-right text-1 ms-1"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-3">
                        <div class="card">
                            <a href="#" style="text-decoration: none">
                                <img class="card-img-top" height="200px" src="img/projects/project-2.jpg" alt="Card Image">
                            </a>
                            <div class="card-body" style="padding:10px;">
                                <a href="#" style="text-decoration: none">
                                    <h6 class="card-title mb-2 text-4 ">News Event Name</h6>
                                </a>
                                <div class="mb-3"><img src=".\img\1-main\calendar-icon.png"><span class="text-2"> 10 April 2021</span></div>
                                <a href="#" class="read-more text-2 float-end mt-3" style="text-decoration: none">Read More<i class="fas fa-chevron-right text-1 ms-1"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-3">
                        <div class="card">
                            <a href="#" style="text-decoration: none">
                                <img class="card-img-top" height="200px" src="img/projects/project-2.jpg" alt="Card Image">
                            </a>
                            <div class="card-body" style="padding:10px;">
                                <a href="#" style="text-decoration: none">
                                    <h6 class="card-title mb-2 text-4 ">News Event Name</h6>
                                </a>
                                <div class="mb-3"><img src=".\img\1-main\calendar-icon.png"><span class="text-2"> 10 April 2021</span></div>
                                <a href="#" class="read-more text-2 float-end mt-3" style="text-decoration: none">Read More<i class="fas fa-chevron-right text-1 ms-1"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-3">
                        <div class="card">
                            <a href="#" style="text-decoration: none">
                                <img class="card-img-top" height="200px" src="img/projects/project-2.jpg" alt="Card Image">
                            </a>
                            <div class="card-body" style="padding:10px;">
                                <a href="#" style="text-decoration: none">
                                    <h6 class="card-title mb-2 text-4 ">News Event Name</h6>
                                </a>
                                <div class="mb-3"><img src=".\img\1-main\calendar-icon.png"><span class="text-2"> 10 April 2021</span></div>
                                <a href="#" class="read-more text-2 float-end mt-3" style="text-decoration: none">Read More<i class="fas fa-chevron-right text-1 ms-1"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-3">
                        <div class="card">
                            <a href="#" style="text-decoration: none">
                                <img class="card-img-top" height="200px" src="img/projects/project-2.jpg" alt="Card Image">
                            </a>
                            <div class="card-body" style="padding:10px;">
                                <a href="#" style="text-decoration: none">
                                    <h6 class="card-title mb-2 text-4 ">News Event Name</h6>
                                </a>
                                <div class="mb-3"><img src=".\img\1-main\calendar-icon.png"><span class="text-2"> 10 April 2021</span></div>
                                <a href="#" class="read-more text-2 float-end mt-3" style="text-decoration: none">Read More<i class="fas fa-chevron-right text-1 ms-1"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-3">
                        <div class="card">
                            <a href="#" style="text-decoration: none">
                                <img class="card-img-top" height="200px" src="img/projects/project-2.jpg" alt="Card Image">
                            </a>
                            <div class="card-body" style="padding:10px;">
                                <a href="#" style="text-decoration: none">
                                    <h6 class="card-title mb-2 text-4 ">News Event Name</h6>
                                </a>
                                <div class="mb-3"><img src=".\img\1-main\calendar-icon.png"><span class="text-2"> 10 April 2021</span></div>
                                <a href="#" class="read-more text-2 float-end mt-3" style="text-decoration: none">Read More<i class="fas fa-chevron-right text-1 ms-1"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-3">
                        <div class="card">
                            <a href="#" style="text-decoration: none">
                                <img class="card-img-top" height="200px" src="img/projects/project-2.jpg" alt="Card Image">
                            </a>
                            <div class="card-body" style="padding:10px;">
                                <a href="#" style="text-decoration: none">
                                    <h6 class="card-title mb-2 text-4 ">News Event Name</h6>
                                </a>
                                <div class="mb-3"><img src=".\img\1-main\calendar-icon.png"><span class="text-2"> 10 April 2021</span></div>
                                <a href="#" class="read-more text-2 float-end mt-3" style="text-decoration: none">Read More<i class="fas fa-chevron-right text-1 ms-1"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-3">
                        <div class="card">
                            <a href="#" style="text-decoration: none">
                                <img class="card-img-top" height="200px" src="img/projects/project-2.jpg" alt="Card Image">
                            </a>
                            <div class="card-body" style="padding:10px;">
                                <a href="#" style="text-decoration: none">
                                    <h6 class="card-title mb-2 text-4 ">News Event Name</h6>
                                </a>
                                <div class="mb-3"><img src=".\img\1-main\calendar-icon.png"><span class="text-2"> 10 April 2021</span></div>
                                <a href="#" class="read-more text-2 float-end mt-3" style="text-decoration: none">Read More<i class="fas fa-chevron-right text-1 ms-1"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <?php include 'include/inc-footer.php'; ?>
    </div>
    <?php include 'include/inc-script.php'; ?>


</body>

</html>