<!DOCTYPE html>
<html>

<head>
    <title>Personal Information</title>
    <?php include 'include/inc-head.php'; ?>
</head>

<body data-plugin-page-transition>

    <div class="body">
        <?php include 'include/inc-header.php'; ?>

        <div role="main" class="main">

            <section class="page-header page-header-modern mb-0">
                <div class="container-xxl">
                    <div class="row">
                        <div class="col-sm-6 align-self-center text-start">
                            <h1 class="text-dark">Personal Information</h1>
                        </div>
                        <div class="col-sm-6 align-self-center text-start text-sm-end">
                            <ul class="breadcrumb d-block">
                                <li><a href="#">Home</a></li>
                                <li class="active">Personal Information</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>

            <div class="container-xxl">
                <div class="row g-0 position-relative">

                    <div class="sidebar col-md-3 col-lg-2">
                        <div class="container-fluid">
                            <p class="my-0">Personal Information</p>
                            <hr class="my-2">
                            <p class="my-0"><a class="text-decoration-none" href="./status-user.php">Course Status</a></p>
                            <hr class="my-2">
                        </div>
                    </div>

                    <div class="content col col-md-9 col-lg-10">
                        <div class="container pb-5 card card-pf">
                            <div class="row justify-content-center ">
                                <div class="col col-md-10 col-lg-8">
                                    <div class=" card-profile ">
                                        <!-- <div class="card-body"> -->
                                        <p class="my-0">Personal Information</p>
                                        <br>
                                        <div class="body-size">

                                            <div class="card card-profile-detail">
                                                <p>Firstname - Lastname <br> <span>อัศวรรณ์ จำเริญสม</span></p>
                                            </div>
                                            <div class="card card-profile-detail">
                                                <p>Employee ID <br><span> 7489287894756</span></p>
                                            </div>
                                            <div class="card card-profile-detail">
                                                <p>Section code <br><span> XXXXXXXXX</span></p>
                                            </div>
                                            <div class="card card-profile-detail">
                                                <p>Section name <br><span>XXXXXXXXX</span></p>
                                            </div>
                                            <div class="card card-profile-detail">
                                                <p>Class level <br><span>XXXXXXXXX</span></p>
                                            </div>
                                            <div class="card card-profile-detail">
                                                <p>Position Description <br><span>XXXXXXXXX</span></p>
                                            </div>
                                        </div>

                                        <!-- </div> -->

                                    </div>
                                </div>
                                <div class=" col-lg-2">
                                    <img class="pf-img" src="img/1-main/profile-image.png">
                                    <div class="card-body text-center" style="padding:10px;">
                                        <button class="col-bt btn btn-main text-4 text-center">edit </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php include 'include/inc-footer.php'; ?>
    </div>
    <?php include 'include/inc-script.php'; ?>


</body>

</html>