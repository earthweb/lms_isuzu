<!DOCTYPE html>
<html>

<head>
    <title>News</title>
    <?php include 'include/inc-head.php'; ?>
    <style>
        .dots span {
            width: 3px;
            height: 3px;
            border-radius: 50%;
            background-color: rgba(8, 8, 8, 0.5);
            display: inline-block;
            margin: 0 10px 15px;
            /* T R-L B */
        }
    </style>
</head>

<body data-plugin-page-transition>

    <div class="body">
        <?php include 'include/inc-header.php'; ?>

        <div role="main" class="main">

            <section class="page-header page-header-modern mb-0">
                <div class="container-xxl">
                    <div class="row">
                        <div class="col-sm-6 align-self-center text-start">
                            <h1 class="text-dark">News</h1>
                        </div>
                        <div class="col-sm-6 align-self-center text-start text-sm-end">
                            <ul class="breadcrumb d-block">
                                <li><a href="#">Home</a></li>
                                <li class="active">News</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>

            <div class="container py-5">
                <div class="row justify-content-center">
                    <div class="col mb-4">
                        <img class="img-fluid mx-auto d-block my-3" height="auto" src="img/1-main/news-pic.png">
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-lg-8 mb-4">
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi accusantium corporis in dolorem! Dolorum nihil molestias in quod iure molestiae officiis ipsam dicta. Veniam consequatur at, cum est sit qui!</p>
                        <div class="d-flex justify-content-center dots">
                            <span></span><span></span><span></span>
                        </div>
                        <p>1 Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi accusantium corporis in dolorem! Dolorum nihil molestias in quod iure molestiae officiis ipsam dicta. Veniam consequatur at, cum est sit qui!</p>
                        <p>2 Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi accusantium corporis in dolorem! Dolorum nihil molestias in quod iure molestiae officiis ipsam dicta. Veniam consequatur at, cum est sit qui!</p>
                        <p>3 Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi accusantium corporis in dolorem! Dolorum nihil molestias in quod iure molestiae officiis ipsam dicta. Veniam consequatur at, cum est sit qui!</p>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-lg-10 my-4">
                        <img class="img-fluid mx-auto d-block mb-2" height="auto" src="img/1-main/news-pic-2.png">
                        <p class="text-center fst-italic">" reference picture "</p>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-lg-8 my-4">
                        <blockquote>
                            <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Necessitatibus quia, illum, delectus ratione impedit commodi, accusamus dolores beatae tenetur perferendis nobis possimus. Laboriosam cupiditate mollitia esse dolorem officiis necessitatibus unde?</p>
                        </blockquote>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-lg-10 my-4">
                        <span class="border rounded-3 px-3 py-1">#TAG</span>
                        <hr class="mt-5">
                    </div>
                </div>
                    <div class="col-md-3 order-md-1">
                        <a href="./allnews.php">
                            <button type="button" class="btn btn-outline btn-rounded btn-dark text-2 px-4"><i class="fas fa-chevron-left"></i> Back</button>
                        </a>
                    </div>
                </div>
            </div>

        </div>

        <?php include 'include/inc-footer.php'; ?>
    </div>
    <?php include 'include/inc-script.php'; ?>


</body>

</html>