<!DOCTYPE html>
<html>

<head>
    <title>Post-test</title>
    <?php include 'include/inc-head.php'; ?>
    <style>
        hr.style1 {
            border-top: 1px dashed rgba(8, 8, 8, 0.1);
            margin: 10px 0 22px;
        }
    </style>
</head>

<body data-plugin-page-transition>

    <div class="body">
        <?php include 'include/inc-header.php'; ?>

        <div role="main" class="main">

            <section class="page-header page-header-modern mb-0">
                <div class="container-xxl">
                    <div class="row">
                        <div class="col-sm-6 align-self-center text-start">
                            <h1 class="text-dark">Post-test</h1>
                        </div>
                        <div class="col-sm-6 align-self-center text-start text-sm-end">
                            <ul class="breadcrumb d-block">
                                <li><a href="#">Home</a></li>
                                <li><a href="#">All course</a></li>
                                <li class="active">Lesson 1</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>

            <div class="container py-5">
                <div class="row justify-content-center">
                    <div class="col col-lg-7">
                        <div class="card mb-4">
                            <div class="card-body p-4">
                                <div class="text-center">
                                    <img src=".\img\1-main\check_circle_24px.png">
                                    <h6 class="text-3 mt-2">Passed</h6>
                                    <h4>Test Result Lesson 1</h4>
                                    <h5>Course Name : XXXXXXXXXXXXXXX</h5>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col">
                                        <h6>Question</h6>
                                    </div>
                                    <div class="col text-end">
                                        <h6 class="text-red">15 question</h6>
                                    </div>
                                </div>
                                <hr class="style1">
                                <div class="row">
                                    <div class="col">
                                        <h6>Time</h6>
                                    </div>
                                    <div class="col  text-end">
                                        <h6 class="text-red">00:30:00</h6>
                                    </div>
                                </div>
                                <hr class="style1">
                                <div class="row">
                                    <div class="col">
                                        <h6>Time used</h6>
                                    </div>
                                    <div class="col  text-end">
                                        <h6 class="text-red">00:15:00</h6>
                                    </div>
                                </div>
                                <hr class="style1">
                                <div class="row">
                                    <div class="col">
                                        <h6>Full Score</h6>
                                    </div>
                                    <div class="col  text-end">
                                        <h6 class="text-red">15 point</h6>
                                    </div>
                                </div>
                                <hr class="style1">
                                <div class="row">
                                    <div class="col">
                                        <h6>Score</h6>
                                    </div>
                                    <div class="col  text-end">
                                        <h6 class="text-red">13 point</h6>
                                    </div>
                                </div>
                                <hr class="style1">
                                <div class="row">
                                    <div class="col">
                                        <h6>Percentage</h6>
                                    </div>
                                    <div class="col  text-end">
                                        <h6 class="text-red">90 %</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="text-center mb-5">
                            <a href="./coursedetail.php" class="btn btn-main text-decoration-none px-5">
                                Return
                            </a>
                        </div>
                    </div>
                </div>

            </div>

            <?php include 'include/inc-footermain.php'; ?>
        </div>
        <?php include 'include/inc-script.php'; ?>


</body>

</html>