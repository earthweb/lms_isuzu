<footer  class="border border-end-0 border-start-0 border-bottom-0 border-color-light-3 mt-0">
    <div class="bg-footer">
        
    </div>
    <div class="copyright bg-main">
        <div class="container text-center py-2">
            <p class="mb-0 text-2 text-light">Copyright © 2021 ISUZU Motors (Thailand) Co., Ltd.</p>
        </div>
    </div>
</footer>