<header id="header" class="header-effect-shrink" data-plugin-options="{'stickyEnabled': true, 'stickyEffect': 'shrink', 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': false, 'stickyChangeLogo': true, 'stickyStartAt': 120, 'stickyHeaderContainerHeight': 60}">
    <div class="header-body border-top-0 header-border">
        <div class="header-top">
            <div class="container-xxl">
                <div class="header-row py-2">
                    <div class="header-column justify-content-start">
                        <div class="header-row">
                            <a href="index.php">
                                <img alt="ISUZU" width="auto" height="auto" src="img/1-main/logo.png">
                            </a>
                        </div>
                    </div>
                    <div class="header-column">
                        <div class="header-row justify-content-end">
                            <div class="header-nav-features">
                                <div class="header-nav-features-search-reveal-container">
                                    <li class="language-change nav-item dropdown nav-item-left-border d-none d-sm-block nav-item-left-border-remove nav-item-left-border-md-show">
                                        <a class="nav-link main-flag" href="#" role="button" id="dropdownLanguage" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <img src="img/1-main/us-flag.png" />
                                        </a>
                                        <div class="dropdown-menu" aria-labelledby="dropdownLanguage">
                                            <a class="dropdown-item" href="#"><img src="img/1-main/us-flag.png" alt="EN" /> US</a>
                                            <a class="dropdown-item" href="#"><img src="img/1-main/th-flag.png" alt="TH" /> TH</a>
                                        </div>
                                    </li>
                                    <a href="login.php" target="_blank" class="btn-login line-height-2 ms-2 d-sm-inline-block">
                                        <img src="img/1-main/login-icon.png" alt=""><span class="d-none d-sm-inline"> Login</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-container container-xxl">
            <div class="header-row">
                <div class="header-column justify-content-end">
                    <div class="header-row">

                        <div class="header-nav header-nav-links order-2 order-lg-1">
                            <div class="menu-main header-nav-main header-nav-main-square header-nav-main-effect-2 header-nav-main-sub-effect-1">
                                <nav class="collapse">
                                    <ul class="nav nav-pills" id="mainNav">
                                        <li class="">
                                            <a class="px-0 pe-lg-3" href="index.php">
                                                <img src="img/1-main/home-black.png" class="icon-m" alt="">
                                                <img src="img/1-main/home-red.png" class="icon-w" alt="">
                                            </a>
                                            <a class="d-inline d-lg-none" href="index.php">Home</a>
                                        </li>
                                        <li class=""><a class="" href="#">Course</a></li>
                                        <li class=""><a class="" href="#">News</a></li>
                                        <li class=""><a class="" href="#">FAQ</a></li>
                                        <li class=""><a class="" href="#">How to use</a></li>
                                        <li class=""><a class="" href="#">Contact Us</a></li>
                                    </ul>
                                </nav>
                            </div>
                            <button class="btn header-btn-collapse-nav" data-bs-toggle="collapse" data-bs-target=".header-nav-main nav">
                                <i class="fas fa-bars"></i>
                            </button>
                        </div>

                        <div class="header-nav-features header-nav-features-no-border header-nav-features-lg-show-border order-1 order-lg-2 p-0 m-0">
                            <div class="header-nav-feature header-nav-features-search d-inline-flex">
                                <!-- <a href="#" class="header-nav-features-toggle text-decoration-none" data-focus="headerSearch"><i class="fas fa-search header-nav-top-icon"></i></a>
                                <div class="header-nav-features-dropdown" id="headerTopSearchDropdown">
                                    <form role="search" action="page-search-results.html" method="get">
                                        <div class="simple-search input-group">
                                            <input class="form-control text-1" id="headerSearch" name="q" type="search" value="" placeholder="Search...">
                                            <button class="btn" type="submit">
                                                <i class="fas fa-search header-nav-top-icon"></i>
                                            </button>
                                        </div>
                                    </form>
                                </div> -->

                                <form role="search" action="page-search-results.html" method="get">
                                        <div class="simple-search input-group">
                                            <input class="form-control text-1" id="headerSearch" name="q" type="search" value="" placeholder="Search...">
                                            <button class="btn" type="submit">
                                                <i class="fas fa-search header-nav-top-icon"></i>
                                            </button>
                                        </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</header>